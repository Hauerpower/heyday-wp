<?php get_header(); ?>
    <!-- First Section -->
    <section class="offer-banner" style="background-image: url('<?php the_field( 'banner-glowny-zdjecie' ); ?>');">
        <div class="grid-container">
            <div class="grid-x">
                <div class="cell medium-12">
                    <h1 class="big-title"><?php the_field('banner-glowny-tytul'); ?></h1>
                </div>
            </div>
        </div>
    </section>
<?php get_footer();