<?php get_header(); ?>
    <!-- First Section -->
    <section class="production-baner section" style="background-image: url('<?php the_field('pierwsza-sekcja-zdjecie'); ?>');">
        <div class="grid-container">
            <div>
                <h1 class="big-title"><?php the_field('pierwsza-sekcja-tytul'); ?></h1>
            </div>
        </div>
    </section>
    <!-- Second Section -->
    <section class="produkcja-main text-center section">
        <div class="grid-container">
            <div class="grid-x produkcja-desktop">
                <div class="cell medium-3">
                    <div class="produkcja-text produkcja-text-1">
                        <h3 class="produkcja-title"><?php the_field('druga-sekcja-material-podtytul'); ?></h3>
                        <?php the_field('druga-sekcja-material-opis'); ?>
                    </div>
                    <div class="produkcja-text produkcja-text-3">
                        <div class="grid-x">
                            <div class="cell small-6"></div>
                            <div class="cell small-4 small-offset-2">
                                <h3 class="produkcja-title"><?php the_field('druga-sekcja-dodatki-podtytul'); ?></h3>
                            </div>
                        </div>
                        <div class="grid-x">
                            <div class="cell small-6">
                                <?php the_field('druga-sekcja-dodatki-lewy-opis'); ?>
                            </div>
                            <div class="cell small-4 small-offset-2">
                                <?php the_field('druga-sekcja-dodatki-prawy-opis'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cell medium-5">
                    <img src="<?php echo ( $uri = get_stylesheet_directory_uri() ); ?>/assets/img/process.png" alt="<?php esc_attr_e( 'Proces', 'heyday' ); ?>">
                </div>
                <div class="cell medium-4">
                    <div class="produkcja-text produkcja-text-2">
                        <h3 class="produkcja-title"><?php the_field('druga-sekcja-projekt-podtytul'); ?></h3>
                        <?php the_field('druga-sekcja-projekt-opis'); ?>
                    </div>
                    <div class="produkcja-text produkcja-text-4">
                        <h3 class="produkcja-title"><?php the_field('druga-sekcja-realizacja-podtytul'); ?></h3>
                        <?php the_field('druga-sekcja-realizacja-opis'); ?>
                    </div>
                </div>
            </div>
            <div class="grid-x produkcja-mobile">
                <div class="produkcja-mobile-box">
                    <div class="abs-circle"><img src="<?php echo ( $uri ); ?>/assets/img/Material.svg" alt="<?php esc_attr_e( 'Material', 'heyday' ); ?>"></div>
                    <div class="produkcja-text produkcja-text-1">
                        <h3 class="produkcja-title"><?php the_field('druga-sekcja-material-podtytul'); ?></h3>
                        <?php the_field('druga-sekcja-material-opis'); ?>
                    </div>
                </div>
                <div class="produkcja-mobile-box">
                    <div class="abs-circle"><img src="<?php echo ( $uri ); ?>/assets/img/Projekt.svg" alt="<?php esc_attr_e( 'Projekt', 'heyday' ); ?>"></div>
                    <div class="produkcja-text produkcja-text-2">
                        <h3 class="produkcja-title"><?php the_field('druga-sekcja-projekt-podtytul'); ?></h3>
                        <?php the_field('druga-sekcja-projekt-opis'); ?>
                    </div>
                </div>
                <div class="produkcja-mobile-box">
                    <div class="abs-circle"><img src="<?php echo ( $uri ); ?>/assets/img/Dodatki.svg" alt="<?php esc_attr_e( 'Dodatki', 'heyday' ); ?>"></div>
                    <div class="produkcja-text produkcja-text-3">
                        <h3 class="produkcja-title"><?php the_field('druga-sekcja-dodatki-podtytul'); ?></h3>
                        <div class="grid-x">
                            <?php the_field('druga-sekcja-dodatki-opis-mobile'); ?>
                        </div>
                    </div>
                </div>
                <div class="produkcja-mobile-box">
                    <div class="abs-circle"><img src="<?php echo ( $uri ); ?>/assets/img/Realizacja.svg" alt="<?php esc_attr_e( 'Realizacja', 'heyday' ); ?>"></div>
                    <div class="produkcja-text produkcja-text-4">
                        <h3 class="produkcja-title"><?php the_field('druga-sekcja-realizacja-podtytul'); ?></h3>
                        <?php the_field('druga-sekcja-realizacja-opis'); ?>
                    </div>
                </div>
            </div>
        </div>        
    </section>
    <!-- Third Section -->
    <section class="about-last production-last">
        <div class="grid-container">
            <h2 class="big-title"><?php the_field( 'trzecia-sekcja-tytul' ); ?></h2>
                <div class="grid-x">
                    <div class="cell medium-6 padding-right-100 ">
                        <?php the_field('trzecia-sekcja-lewy-opis'); ?>
                    </div>
                    <div class="cell medium-6 padding-left-100 right">
                        <div>
                            <?php the_field('trzecia-sekcja-prawy-opis'); ?>
                        </div>
                    </div>
                </div>
            </div>
    </section>
<?php get_footer();