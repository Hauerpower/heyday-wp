<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package heyday
 */
?>

    </div><!-- /.wrap -->
    <div class="footer-big">
        <div class="grid-container">
            <div class="grid-x">
                <div class="cell my-medium-6 footer-left">
                    <h2 class="big-title"><?php the_field('sekcja-kontakt-opis', 'option'); ?></h2>
                </div>
                <div class="cell my-medium-6 footer-form-box">
                    <div class="footer-form">
                        <div>
                            <h2 class="form-title big-title"><?php _e( 'Napisz do nas!', 'heyday' ); ?></h2>
                            <?php echo do_shortcode( '[contact-form-7 id="50"]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-white">
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="cell small-6 medium-4 my-medium-3 footer-white-box">
                    <div class="first-box-inside">
                        <p><?php printf( esc_html__('&copy; %d HEYDAY - studio reklamy Kraków', 'heyday'), date( 'Y' ) ); ?></p>
                        <p><?php _e( 'ul. Szara 12/1, 30-820 Kraków', 'heyday' ); ?></p>
                    </div>
                </div>
                <div class="cell small-6 medium-4 my-medium-6 footer-white-box middle flex">
                    <div class="middle-inside">
                        <div class="first">
                            <div class="flex">
                                <img src="<?php echo ( $uri = get_stylesheet_directory_uri() ); ?>/assets/img/Inteligentny obiekt wektorowy kopia 2.svg" alt="<?php esc_attr_e( 'Email', 'heyday' ) ?>" />
                                <p><a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a></p>
                            </div>                            
                        </div>
                        <div>
                            <div class="flex">
                                <img src="<?php echo $uri; ?>/assets/img/Inteligentny obiekt wektorowy kopia 2a.svg" alt="<?php esc_attr_e( 'Telefon', 'heyday' ); ?>" />
                                <p><a href="tel:<?php the_field( 'telefon', 'option'); ?>"><?php the_field( 'telefon', 'option'); ?></a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cell medium-4 my-medium-3 footer-white-box last">
                    <div class="inside">
                        <p><?php _e( 'Zobacz co u nas:', 'heyday' ); ?></p>
                        <?php
                        wp_nav_menu(
                            array(
                                'theme_location' => 'social-menu',
                                'menu_id' => 'social-menu'
                            )
                        );
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- /#page -->
<?php wp_footer(); ?>
</body>
</html>