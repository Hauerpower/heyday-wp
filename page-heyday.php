<?php get_header(); ?>
    <!-- First Section -->
    <section class="main-baner">
        <div class="grid-container">
            <h1 class="big-title"><?php the_field('banner-glowny-tytul'); ?></h1>
            <div class="p-box">
                <?php the_field('banner-glowny-lista'); ?>
            </div>
            <div class="flex">
                <div class="position-relative">
                    <a href="<?php the_field('banner-glowny-adres-url'); ?>" class="button-green"><?php the_field('banner-glowny-tekst-przycisku'); ?></a>
                    <img src="<?php echo ( $uri = get_stylesheet_directory_uri() ); ?>/assets/img/arrow-right.svg" alt="<?php esc_attr_e( 'Strzałka', 'heyday' ); ?>" class="position-absolute arrow-to-button">
                </div>
            </div>
        </div>
    </section>
    <!-- Second Section -->
    <section class="main-second section position-relative">
        <div class="price position-absolute">
            <div class="price-circle">
                <span class="price-from"><?php _e( 'Ceny od', 'heyday' ); ?></span>
                <span class="price-value"><?php the_field('druga-sekcja-cena'); ?></span>
                <span class="price-small">/<?php _e( 'szt', 'heyday' ); ?></span>
            </div>
        </div>
        <h2 class="text-center title-30"><?php the_field('druga-sekcja-tytul'); ?></h2>
        <div class="grid-x small-up-2 medium-up-3 large-up-5 cells-box grid-padding-x grid-padding-y">
            <?php echo do_shortcode('[products limit="5" columns="5" orderby="date" order="ASC" visibility="visible"]'); ?>
            <div class="cell button-green-cell">
                <div class="flex button-green-box">
                    <div class="position-relative">
                        <a href="<?php the_field('trzecia-sekcja-adres-url'); ?>" class="button-green"><?php the_field( 'trzecia-sekcja-tekst-przycisku' ); ?></a>
                        <img src="<?php echo $uri; ?>/assets/img/arrow-right.svg" alt="<?php esc_attr_e( 'Strzalka', 'heyday' ); ?>" class="position-absolute arrow-to-button">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Third Section -->
    <section class="main-baner main-baner-second section">
        <div class="grid-container">
            <h1 class="big-title"><?php the_field('trzecia-sekcja-tytul'); ?></h1>
            <div class="p-box">
                <?php the_field('trzecia-sekcja-opis'); ?>
            </div>
            <div class="flex">
                <div class="position-relative">
                    <a href="<?php the_field('trzecia-sekcja-adres-url'); ?>" class="button-white"><?php the_field('trzecia-sekcja-tekst-przycisku'); ?></a>
                    <img src="<?php echo $uri; ?>/assets/img/green-arrow.svg" alt="<?php esc_attr_e( 'Strzalka', 'heyday' ); ?>" class="position-absolute arrow-to-button">
                </div>
            </div>
        </div>
    </section>
    <!-- Fourth Section -->
    <section class="section main-last">
        <div class="grid-container">
            <h2 class="title-30 text-center"><?php the_field('czwarta-sekcja-tytul'); ?></h2>
            <div class="last-items-box last-items-box-big">
                <div class="last-items-box">
                    <div class="last-item">
                        <div class="flex center small-flex">
                            <img src="<?php echo $uri; ?>/assets/img/material.png" alt="<?php esc_attr_e( 'Materiał', 'heyday' ); ?>">
                            <p><?php the_field('czwarta-sekcja-proces-1-tytul'); ?></p>
                        </div>
                        <p><?php the_field( 'czwarta-sekcja-proces-1-opis' ); ?></p>
                    </div>
                    <img class="arrow-box arrow-right" src="<?php echo $uri; ?>/assets/img/arrow-right.png" alt="<?php esc_attr_e( 'Strzałka', 'heyday' ); ?>">
                    <img class="arrow-box arrow-down" src="<?php echo $uri; ?>/assets/img/arrow-down.png" alt="<?php esc_attr_e( 'Right Down', 'heyday' ); ?>">
                    <div class="last-item">
                        <div class="flex center small-flex">
                            <img src="<?php echo $uri; ?>/assets/img/projekt.png" alt="<?php esc_attr_e( 'Projekt', 'heyday' ); ?>">
                            <p><?php the_field('czwarta-sekcja-proces-2-tytul'); ?></p>
                        </div>
                        <p><?php the_field( 'czwarta-sekcja-proces-2-opis' ); ?></p>
                    </div>
                    <img class="arrow-box arrow-right" src="<?php echo $uri; ?>/assets/img/arrow-right.png" alt="<?php esc_attr_e( 'Strzałka', 'heyday' ); ?>">
                    <img class="arrow-box arrow-down" src="<?php echo $uri; ?>/assets/img/arrow-down.png" alt="<?php esc_attr_e( 'Right Down', 'heyday' ); ?>">
                    <div class="last-item">
                        <div class="flex center small-flex">
                            <img src="<?php echo $uri; ?>/assets/img/dodatki.png" alt="<?php esc_attr_e( 'Dodatki', 'heyday' ); ?>">
                            <p><?php the_field('czwarta-sekcja-proces-3-tytul'); ?></p>
                        </div>
                        <p><?php the_field( 'czwarta-sekcja-proces-3-opis' ); ?></p>
                    </div>
                    <img class="arrow-box arrow-right" src="<?php echo $uri; ?>/assets/img/arrow-right.png" alt="<?php esc_attr_e( 'Strzałka', 'heyday' ); ?>">
                    <img class="arrow-box arrow-down" src="<?php echo $uri; ?>/assets/img/arrow-down.png" alt="<?php esc_attr_e( 'Right Down', 'heyday' ); ?>">
                    <div class="last-item">
                        <div class="flex center small-flex">
                            <img src="<?php echo $uri; ?>/assets/img/realizacja.png" alt="<?php esc_attr_e( 'Realizacja', 'heyday' ); ?>">
                            <p><?php the_field('czwarta-sekcja-proces-4-tytul'); ?></p>
                        </div>
                        <p><?php the_field( 'czwarta-sekcja-proces-4-opis' ); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php get_footer();