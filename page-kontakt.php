<?php get_header(); ?>
    <!-- First Section -->
    <section class="kontakt-first">
        <div class="grid-container">
            <div class="grid-x">
                <div class="cell my-medium-5 left">
                    
                </div>
                <div class="cell my-medium-7 padding-top-bottom padding-left-60 right">
                    <?php the_field('kontakt-opis'); ?>
                </div>
            </div>
        </div>
    </section>
    <section class="kontakt-main">
        <div class="grid-container">
            <div class="grid-x kontakt-box">
                <div class="cell my-medium-5 padding-left-100">
                    <h2 class="konakt-title"><?php _e( 'HEYDAY', 'heyday' ); ?></h2>
                </div>
                <div class="cell my-medium-7 padding-left-60 ">
                    <div class="grid-x grid-padding-x">
                        <div class="cell small-6">
                            <div class="flex">
                                <div class="img-box"></div>
                                <div>
                                    <?php the_field('dane-firmy'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="cell small-6">
                            <div class="flex">
                                <div class="img-box">
                                    <img src="<?php echo ( $uri = get_stylesheet_directory_uri() ); ?>/assets/img/Inteligentny obiekt wektorowy kopia 2.svg" alt="<?php esc_attr_e( 'Email', 'hayday' ); ?>">
                                </div>
                                <div>
                                    <a href="mailto:<?php the_field('adres-email-firma'); ?>" class="email"><?php the_field('adres-email-firma'); ?></a>
                                </div>
                            </div>
                            <div class="flex">
                                <div class="img-box">
                                    <img src="<?php echo ( $uri ); ?>/assets/img/Inteligentny obiekt wektorowy kopia 2a.svg" alt="<?php esc_attr_e( 'Telefon', 'hayday' ); ?>">
                                </div>
                                <div>
                                    <p><a href="tel:<?php the_field('firma-tel-1'); ?>"><?php the_field('firma-tel-1'); ?></a></p>
                                    <p><a href="tel:<?php the_field('firma-tel-2'); ?>"><?php the_field('firma-tel-2'); ?></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-x kontakt-box">
                <div class="cell my-medium-5 padding-left-100">
                    <h2 class="konakt-title"><?php _e( 'BIURO', 'heyday' ); ?></h2>
                </div>
                <div class="cell my-medium-7 padding-left-60 ">
                    <div class="grid-x grid-padding-x">
                        <?php while( have_rows('biuro-pracownicy') ): the_row(); ?>
                            <?php
                            $in = get_sub_field('biuro-imie-nazwisko');
                            $telefon = get_sub_field('biuro-telefon');
                            $email = get_sub_field('biuro-email');
                            ?>
                            <div class="cell small-6">                            
                                <div class="flex">
                                    <div class="img-box"></div>
                                    <div>
                                        <h3 class="title-19"><?php echo $in; ?></h3>
                                    </div>
                                </div>
                                <div class="flex">
                                    <div class="img-box">
                                        <img src="<?php echo ( $uri ); ?>/assets/img/Inteligentny obiekt wektorowy kopia 2.svg" alt="<?php esc_attr_e( 'Email', 'hayday' ); ?>">
                                    </div>
                                    <div>
                                        <a href="mailto:<?php echo $email; ?>" class="email"><?php echo $email; ?></a>
                                    </div>
                                </div>
                                <div class="flex">
                                    <div class="img-box">
                                        <img src="<?php echo ( $uri ); ?>/assets/img/Inteligentny obiekt wektorowy kopia 2a.svg" alt="<?php esc_attr_e( 'Telefon', 'hayday' ); ?>">
                                    </div>
                                    <div>
                                        <p><a href="tel:<?php echo $telefon; ?>"><?php echo $telefon; ?></a></p>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
            <div class="grid-x kontakt-box">
                <div class="cell my-medium-5 padding-left-100">
                    <h2 class="konakt-title"><?php _e( 'STUDIO GRAFICZNE', 'heyday' ); ?></h2>
                </div>
                <div class="cell my-medium-7 padding-left-60 ">
                    <div class="grid-x grid-padding-x">
                        <?php while( have_rows('studio-graficzne-pracownicy') ): the_row(); ?>
                            <?php
                            $in = get_sub_field('studio-graficzne-imie-nazwisko');
                            $telefon = get_sub_field('studio-graficzne-telefon');
                            $email = get_sub_field('studio-graficzne-email');
                            ?>
                            <div class="cell small-6">                            
                                <div class="flex">
                                    <div class="img-box"></div>
                                    <div>
                                        <h3 class="title-19"><?php echo $in; ?></h3>
                                    </div>
                                </div>
                                <div class="flex">
                                    <div class="img-box">
                                        <img src="<?php echo ( $uri ); ?>/assets/img/Inteligentny obiekt wektorowy kopia 2.svg" alt="<?php esc_attr_e( 'Email', 'hayday' ); ?>">
                                    </div>
                                    <div>
                                        <a href="mailto:<?php echo $email; ?>" class="email"><?php echo $email; ?></a>
                                    </div>
                                </div>
                                <div class="flex">
                                    <div class="img-box">
                                        <img src="<?php echo ( $uri ); ?>/assets/img/Inteligentny obiekt wektorowy kopia 2a.svg" alt="<?php esc_attr_e( 'Telefon', 'hayday' ); ?>">
                                    </div>
                                    <div>
                                        <p><a href="tel:<?php echo $telefon; ?>"><?php echo $telefon; ?></a></p>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php get_footer();