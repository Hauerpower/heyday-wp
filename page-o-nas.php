<?php get_header(); ?>
    <!-- First Section -->
    <section class="about-first">
        <div class="grid-x">
            <div class="cell medium-6 large-5 left small-2rem"></div>
            <div class="cell medium-6 large-7 grid-container-right stretch-right padding-top-bottom padding-left-100 padding-right-100">
                <div>
                    <h1 class="big-title">
                        <?php the_field('o-nas-banner-glowny-tytul'); ?>
                    </h1>
                    <h3 class="small-title">
                        <?php the_field('o-nas-banner-glowny-podtytul'); ?>
                    </h3>
                    <?php the_field('o-nas-banner-glowny-opis'); ?>
                </div>
            </div>
        </div>
    </section>
    <section class="about-second">
        <div class="grid-container">
            <div class="flex center about-second-inside">
                <img src="<?php the_field( 'o-nas-druga-sekcja-torba-lewa' ); ?>" alt="<?php esc_attr_e( 'Torba', 'heyday' ); ?>">
                <div class="about-second-inside-middle">
                    <h3 class="small-title"><?php the_field('o-nas-druga-sekcja-tytul'); ?></h3>
                    <?php the_field('o-nas-druga-sekcja-opis'); ?>
                </div>
                <img src="<?php the_field( 'o-nas-druga-sekcja-torba-prawa' ); ?>" class="about-second-inside-right" alt="<?php esc_attr_e( 'Torba', 'heyday' ); ?>">
            </div>
        </div>
    </section>
    <section class="about-third">
        <div class="grid-container">
            <div class="grid-x">
                <div class="cell medium-5 large-6 padding-right-100 left grid-container-left stretch-left">
                    <h2 class="small-title"><?php the_field( 'o-nas-trzecia-sekcja-lewa-strona-tytul' ); ?></h2>
                    <?php the_field( 'o-nas-trzecia-sekcja-lewa-strona-opis' ); ?>
                    <div class="flex">
                        <div class="position-relative">
                            <a href="<?php the_field('o-nas-trzecia-sekcja-lewa-strona-adres-url'); ?>" class="button-green"><?php the_field( 'o-nas-trzecia-sekcja-lewa-strona-tekst-przycisku' ); ?></a>
                            <img src="<?php echo ( $uri = get_stylesheet_directory_uri() ); ?>/assets/img/arrow-right.svg" alt="<?php esc_attr_e( 'Strzałka', 'heyday' ); ?>" class="position-absolute arrow-to-button">
                        </div>
                    </div>
                </div>
                <div class="cell medium-7 large-6 flex right">
                    <div class="grid-x">
                        <div class="small-8 medium-6">
                            <div class="third-box">
                                <h2 class="small-title"><?php the_field('o-nas-trzecia-sekcja-prawa-strona-tytul'); ?></h2>
                                <?php the_field('o-nas-trzecia-sekcja-prawa-strona-opis'); ?>
                                <div class="flex">
                                    <div class="position-relative">
                                        <a href="<?php the_field('o-nas-trzecia-sekcja-prawa-strona-adres-url'); ?>" class="button-green"><?php the_field('o-nas-trzecia-sekcja-prawa-strona-tekst-przycisku'); ?></a>
                                        <img src="<?php echo ( $uri ); ?>/assets/img/arrow-right.svg" alt="<?php esc_attr_e( 'Strzałka', 'heyday' ); ?>" class="position-absolute arrow-to-button">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="small-4 medium-6 bag-box position-relative">
                            <img src="<?php the_field( 'o-nas-trzecia-sekcja-prawa-strona-zdjecie' ); ?>" />
                            <div class="price position-absolute">
                                <div class="price-circle">
                                    <span class="price-from"><?php _e( 'Ceny od', 'heyday' ); ?></span>
                                    <span class="price-value"><?php the_field('trzecia-sekcja-cena'); ?></span>
                                    <span class="price-small">/<?php _e( 'szt', 'heyday' ); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </section>
    <section class="about-fourth position-relative">
        <div class="gray-back"></div>
        <div class="grid-container">
            <div class="grid-x">
                <div class="cell small-medium-6 padding-top-bottom padding-right-100">
                    <h2 class="big-title"><?php the_field('o-nas-czwarta-sekcja-tytul'); ?></h2>
                    <h3 class="small-title"><?php the_field('o-nas-czwarta-sekcja-podtytul'); ?></h3>
                    <?php the_field('o-nas-czwarta-sekcja-opis'); ?>
                    <div class="flex">
                        <div class="position-relative">
                            <a href="<?php the_field('o-nas-czwarta-sekcja-adres-url'); ?>" class="button-green"><?php the_field('o-nas-czwarta-sekcja-tekst-przycisku'); ?></a>
                            <img src="<?php echo ( $uri ); ?>/assets/img/arrow-right.svg" alt="<?php esc_attr_e( 'Strzałka', 'heyday' ); ?>" class="position-absolute arrow-to-button">
                        </div>
                    </div>
                </div>
                <div class="cell small-medium-6 image small-medium-250 order-1-small-medium small-medium-2rem" style="background-image: url('<?php the_field('o-nas-czwarta-sekcja-zdjecie'); ?>');"></div>
            </div>
        </div>
    </section>
    <section class="about-fifth position-relative">
        <div class="gray-back"></div>
        <div class="grid-x">
            <div class="cell small-medium-5 image small-medium-250 small-medium-2rem" style="background-image: url('<?php the_field('o-nas-piata-sekcja-zdjecie'); ?>);"></div>
            <div class="cell small-medium-7 grid-container-right stretch-right padding-left-100 padding-top-bottom">
                <h2 class="big-title"><?php the_field('o-nas-piata-sekcja-tytul'); ?></h2>
                <h3 class="small-title"><?php the_field('o-nas-piata-sekcja-podtytul') ?></h3>
                <?php the_field('o-nas-piata-sekcja-opis'); ?>
                <div class="flex">
                    <div class="position-relative">
                        <a href="<?php the_field('o-nas-piata-sekcja-adres-url'); ?>" class="button-green"><?php the_field('o-nas-piata-sekcja-tekst-przycisku'); ?></a>
                        <img src="<?php echo ( $uri ); ?>/assets/img/arrow-right.svg" alt="<?php esc_attr_e( 'Strzałka', 'heyday' ); ?>" class="position-absolute arrow-to-button">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="about-last" style="background-image: url('<?php the_field('o-nas-szosta-sekcja-zdjecie'); ?>);">
        <div class="grid-container">
            <h2 class="big-title padding-left-100"><?php the_field('o-nas-szosta-sekcja-tytul'); ?></h2>
            <div class="grid-x">
                <div class="cell medium-6 large-5 padding-left-100">
                    <?php the_field('o-nas-szosta-sekcja-lewy-opis'); ?>
                </div>
                <div class="cell medium-6 large-7 padding-left-100 padding-right-100">
                    <?php the_field('o-nas-szosta-sekcja-prawy-opis'); ?>
                    <div class="flex button-box">
                        <div class="position-relative">
                            <a href="<?php the_field('o-nas-szosta-sekcja-adres-url'); ?>" class="button-white"><?php the_field('o-nas-szosta-sekcja-tekst-przycisku'); ?></a>
                            <img src="<?php echo ( $uri ); ?>/assets/img/arrow-right.svg" alt="<?php esc_attr_e( 'Strzałka', 'heyday' ); ?>" class="position-absolute arrow-to-button">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php get_footer();