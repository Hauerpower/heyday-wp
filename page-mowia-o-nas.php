<?php get_header(); ?>
    <!-- First Section -->
    <section class="production-baner baner-opinie">
        <div class="grid-container">
            <div class="grid-x">
                <div class="cell medium-4">
                    <h1 class="big-title"><?php the_field('banner-glowny-tytul'); ?></h1>
                </div>
                <div class="cell medium-8">
                    <?php the_field('banner-glowny-opis'); ?>
                </div>
            </div>
        </div>
    </section>
    <section class="opinie-main section">
        <div class="grid-container">
            <div class="opinie-grid-box">
                <div class="grid-x">
                    <div class="cell small-2"></div>
                    <div class="cell small-8">
                        <div class="grid-x my-grid-x">
                            <div class="cell small-medium-6 my-medium-4 opinie-cell-box">
                                <div class="opinie-cell text-right">
                                    <h3 class="opinie-title"><?php the_field('tytul-1'); ?></h3>
                                    <?php the_field('opis-1'); ?>
                                </div>
                            </div>
                            <div class="cell small-medium-6 my-medium-5  my-medium-offset-3 opinie-cell-box">
                                <div class="opinie-cell text-right">
                                    <h3 class="opinie-title"><?php the_field('tytul-2'); ?></h3>
                                    <?php the_field('opis-2'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cell small-2">
                        <div class="half-circle"></div>
                    </div>
                </div>
            </div>
            <div class="opinie-grid-box opinie-grid-box-left">
                <div class="grid-x big-box">
                    <div class="cell small-2">
                        <div class="half-circle-left"></div>
                    </div>
                    <div class="cell small-8">
                        <div class="grid-x my-grid-x my-grid-x-left">
                            <div class="cell small-medium-6 my-medium-4 opinie-cell-box">
                                <div class="opinie-cell ">
                                    <h3 class="opinie-title"><?php the_field('tytul-3'); ?></h3>
                                    <?php the_field('opis-3'); ?>
                                </div>
                            </div>
                            <div class="cell  small-medium-6 my-medium-5  my-medium-offset-3 opinie-cell-box">
                                <div class="opinie-cell">
                                    <h3 class="opinie-title"><?php the_field('tytul-4'); ?></h3>
                                    <?php the_field('opis-4'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cell small-2"></div>
                </div>
            </div>
            <div class="opinie-grid-box">
                <div class="grid-x">
                    <div class="cell small-2"></div>
                    <div class="cell small-8">
                        <div class="grid-x my-grid-x">
                            <div class="cell small-medium-6 my-medium-4 opinie-cell-box">
                                <div class="opinie-cell text-right">
                                    <h3 class="opinie-title"><?php the_field('tytul-5'); ?></h3>
                                    <?php the_field('opis-5'); ?>
                                </div>
                            </div>
                            <div class="cell small-medium-6 my-medium-5  my-medium-offset-3 opinie-cell-box">
                                <div class="opinie-cell text-right">
                                    <h3 class="opinie-title"><?php the_field('tytul-6'); ?></h3>
                                    <?php the_field('opis-6'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cell small-2">
                        <div class="half-circle"></div>
                    </div>
                </div>
            </div>
            <div class="opinie-grid-box opinie-grid-box-left">
                <div class="grid-x big-box">
                    <div class="cell small-2">
                        <div class="half-circle-left"></div>
                    </div>
                    <div class="cell small-8">
                        <div class="grid-x my-grid-x my-grid-x-left">
                            <div class="cell small-medium-6 my-medium-4 opinie-cell-box">
                                <div class="opinie-cell ">
                                    <h3 class="opinie-title"><?php the_field('tytul-7'); ?></h3>
                                    <?php the_field('opis-7'); ?>
                                </div>
                            </div>
                            <div class="cell  small-medium-6 my-medium-5  my-medium-offset-3 opinie-cell-box">
                                <div class="opinie-cell">
                                    <h3 class="opinie-title"><?php the_field('tytul-8'); ?></h3>
                                    <?php the_field('opis-8'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cell small-2"></div>
                </div>
            </div>
        </div>
    </section>
<?php get_footer();