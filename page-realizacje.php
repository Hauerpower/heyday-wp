<?php get_header(); ?>
    <!-- First Section -->
    <section class="realizacje section position-relative">
        <div class="position-absolute realization-leafe-1"><img src="<?php echo ( $uri = get_stylesheet_directory_uri() ); ?>/assets/img/Lisc-1.png" alt="<?php esc_attr_e( 'Lisc', 'heyday' ); ?>"></div>
        <div class="position-absolute realization-leafe-2"><img src="<?php echo ( $uri ); ?>/assets/img/Lisc-2.png" alt="<?php esc_attr_e( 'Lisc', 'heyday' ); ?>"></div>
        <div class="grid-container">
            <h1 class="title-30 text-center"><?php the_field('pierwsza-sekcja-tytul'); ?></h1>
            <div class="grid-images">
                <?php
                $i = 1;
                ?>
                <?php while( have_rows('realizacje') ): the_row(); ?>
                    <?php
                    $zdjecie = get_sub_field('realizacja-zdjecie');
                    $nazwa = get_sub_field('realizacja-nazwa-produktu');
                    $opis = get_sub_field('realizacja-opis');
                    ?>
                    <div class="position-relative grid-image image-<?php echo ( $i++ ); ?>" style="background-image: url('<?php echo $zdjecie; ?>');">
                        <div class="position-absolute image-text">
                            <h3 class="grid-image-title"><?php echo $nazwa; ?></h3>
                            <p><?php echo $opis; ?></p>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
<?php get_footer();