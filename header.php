<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package heyday
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="https://gmpg.org/xfn/11">
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <?php wp_body_open(); ?>
        <div id="page" class="site">
            <a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'heyday' ); ?></a>
            <header id="site-header" class="header">
                <div class="grid-container">
                    <div class="grid-x header-wrap align-justify align-middle">
                        <div class="cell logo-wrap shrink">
                            <div class="position-absolute logo-box">
                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo ( $uri = get_stylesheet_directory_uri() ); ?>/assets/img/logo.png" alt="<?php esc_attr_e( 'HeyDay', 'heyday' ) ?>"></a>
                            </div>
                        </div>
                        <div class="cell menu-wrap shrink">
                            <a href="#" id="hamburger">
                                <span></span>
                            </a>
                            <?php
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'primary-menu',
                                    'menu_id' => 'primary-menu',
                                    'menu_class' => 'menu main-menu'
                                )
                            );
                            ?>
                        </div>
                    </div>
                </div>
            </header><!-- #site-header -->
            <div class="wrap">
